import { BrowserRouter, Routes, Route, NavLink } from "react-router-dom";
import Nav from "./Nav";
import AtteendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm.js";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />

      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route
          path="attendees"
          element={<AtteendeesList attendees={props.attendees} />}
        />
        <Route />
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
