import React, { useState, useEffect } from "react";

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [presenterName, setPresenterName] = useState("");
  const [presenterEmail, setPresenterEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      console.log(data);
    }
  };

  const handlePresenterNameChange = (event) => {
    setPresenterName(event.target.value);
  };

  const handlePresenterEmailChange = (event) => {
    setPresenterEmail(event.target.value);
  };

  const handleCompanyNameChange = (event) => {
    setCompanyName(event.target.value);
  };

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const handleSynopsisChange = (event) => {
    setSynopsis(event.target.value);
  };

  const handleConferenceChange = (event) => {
    setConference(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.presenter_name = presenterName;
    data.presenter_email = presenterEmail;
    data.company_name = companyName;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    console.log(conferences);
    const presentationsUrl = `http://localhost:8000${conference}presentations/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(presentationsUrl, fetchConfig);
    if (response.ok) {
      setPresenterName("");
      setPresenterEmail("");
      setCompanyName("");
      setTitle("");
      setSynopsis("");
      setConference("");
    }
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter name"
                  required
                  type="text"
                  name="presenter_name"
                  id="presenter_name"
                  className="form-control"
                  value={presenterName}
                  onChange={handlePresenterNameChange}
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter email"
                  required
                  type="email"
                  name="presenter_email"
                  id="presenter_email"
                  className="form-control"
                  value={presenterEmail}
                  onChange={handlePresenterEmailChange}
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Company name"
                  type="text"
                  name="company_name"
                  id="company_name"
                  className="form-control"
                  value={companyName}
                  onChange={handleCompanyNameChange}
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                  value={title}
                  onChange={handleTitleChange}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  className="form-control"
                  id="synopsis"
                  rows="3"
                  name="synopsis"
                  value={synopsis}
                  onChange={handleSynopsisChange}
                ></textarea>
              </div>
              <div className="mb-3">
                <label htmlFor="conference">Conference</label>
                <select
                  required
                  name="conference"
                  id="conference"
                  className="form-select"
                  value={conference}
                  onChange={handleConferenceChange}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
