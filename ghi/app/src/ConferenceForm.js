import React, { useEffect, useState } from "react";
function ConferenceForm(props) {
  const [name, setName] = useState("");
  const [starts, setStarts] = useState("");
  const [ends, setEnds] = useState("");
  const [description, setDescription] = useState("");
  const [maxPresentations, setMaxPresentations] = useState("");
  const [maxAttendees, setMaxAttendees] = useState("");
  const [locations, setLocations] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState("");

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleStartsChange = (event) => {
    setStarts(event.target.value);
  };
  const handleEndsChange = (event) => {
    setEnds(event.target.value);
  };
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };
  const handleMaxPresentationsChange = (event) => {
    setMaxPresentations(event.target.value);
  };
  const handleMaxAttendeesChange = (event) => {
    setMaxAttendees(event.target.value);
  };
  const handleLocationChange = (event) => {
    setSelectedLocation(event.target.value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = selectedLocation;

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);

    if (response.ok) {
      const newConference = await response.json();
      // Reset the form by setting state variables to empty strings
      setName("");
      setStarts("");
      setEnds("");
      setDescription("");
      setMaxPresentations("");
      setMaxAttendees("");
      setSelectedLocation("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={name}
                onChange={handleNameChange}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStartsChange}
                placeholder="starts"
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
                value={starts}
              />
              <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEndsChange}
                placeholder="ends"
                required
                type="date"
                name="ends"
                id="ends"
                className="form-control"
                value={ends}
              />
              <label htmlFor="ends">End Date</label>
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="description" className="form-label">
                Decription
              </label>
              <textarea
                className="form-control"
                name="description"
                rows="3"
                value={description}
                onChange={handleDescriptionChange}
              ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleMaxPresentationsChange}
                placeholder="Max Presentations"
                required
                type="number"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
                value={maxPresentations}
              />
              <label htmlFor="max_presentations">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleMaxAttendeesChange}
                placeholder="Max Attendees"
                required
                type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
                value={maxAttendees}
              />
              <label htmlFor="max_presentations">Max Attendees</label>
            </div>
            <div className="form-floating mb-3">
              <div className="mb-3">
                <select
                  required
                  name="location"
                  id="location"
                  className="form-select"
                  value={selectedLocation}
                  onChange={handleLocationChange}
                >
                  <option value="location">Location</option>
                  {locations.map((location) => (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
